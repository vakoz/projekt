/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.truck.service;

/**
 *
 * @author vakoz
 */
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import com.truck.domain.Truck;

@ApplicationScoped
public class TruckRepositoryImpl implements TruckRepository {

	private int count;
	private Map<String, Truck> idToTruckMap = new HashMap<String, Truck>();

	public TruckRepositoryImpl()  {
		synchronized (this) {
			trucks(truck("W300", "Scania", "1999", "manual", "GPS, power steering","screw that - better buy Volvo", "27000", "0"),
					truck("VNL 780", "Volvo", "2015", "auto", "ABS, power steering, GPS, heated seats","If you want "
                                                + "dependable over-the-road transportation without compromising"
                                                + " on comfort or quality, look no further than our flagship, "
                                                + "the VNL 780", "136850", "0"));
		}
	}

	private Truck truck(String name, String make, String xdot, String transmission,
                String extras, String description, String xprice, String xupdate)  {

                int dot;
                int price;
                
                int update;
                
                dot = Integer.parseInt(xdot);
                price = Integer.parseInt(xprice);
                update = Integer.parseInt(xupdate);
                
                if(update == 1)
                    count--;
			
		return new Truck("" + (count++), name, make, dot, transmission, extras,
                description, price);
		
	}

	private void trucks(Truck... trucks) {
		for (Truck truck : trucks) {
			doAddTruck(truck);
		}
	}

	private void doAddTruck(Truck truck) {
		synchronized (this) {
			this.idToTruckMap.put(truck.getId(), truck);
		}
	}

	@Override
	public Truck lookupTruckById(String id)  {
		synchronized (this) {
			return this.idToTruckMap.get(id).cloneMe();
		}
	}

	@Override
	public void addTruck(String name, String make, String dot, String transmission,
                String extras, String description, String price)  {
		doAddTruck(truck(name, make, dot, transmission, extras,
                description, price, "0"));
	}

	@Override
	public void updateTruck(String id, String name, String make, String dot, 
                String transmission, String extras, String description, String price) {
		Truck truck = truck(name, make, dot, transmission, extras,
                description, price, "1");
		synchronized (this) {
			truck.setId(id);
			this.idToTruckMap.put(id, truck);
		}
	}

	private List<Truck> doListTrucks()  {
		List<Truck> trucks;
		synchronized (this) {

			trucks = new ArrayList<Truck>(this.idToTruckMap.size());
			for (Truck truck : this.idToTruckMap.values()) {
				trucks.add(truck.cloneMe());
			}
		}
		return trucks;
	}
	
	public List<Truck> listTrucks(final String sort) {
		
		List<Truck> trucks = doListTrucks();
                
                if (sort.contains("id")) {
		Collections.sort(trucks, new Comparator<Truck>() { 
			public int compare(Truck truckA, Truck truckB) {                        
				return truckA.getId().compareToIgnoreCase(truckB.getId());
			}
		});
                }
                
                if (sort.contains("name")) {
		Collections.sort(trucks, new Comparator<Truck>() { 
			public int compare(Truck truckA, Truck truckB) {                        
				return truckA.getName().compareToIgnoreCase(truckB.getName());
			}
		});
                }
                
                if (sort.contains("make")) {
		Collections.sort(trucks, new Comparator<Truck>() { 
			public int compare(Truck truckA, Truck truckB) {                        
				return truckA.getMake().compareToIgnoreCase(truckB.getMake());
			}
		});
                }
                
                if (sort.contains("yop")) {
		Collections.sort(trucks, new Comparator<Truck>() { 
			public int compare(Truck truckA, Truck truckB) {
				//return Integer.toString(truckA.getDot()).compareToIgnoreCase(Integer.toString(truckB.getDot()));
                                return truckA.getDot() - truckB.getDot();
                        }
		});
                }
                
                if (sort.contains("trans")) {
		Collections.sort(trucks, new Comparator<Truck>() { 
			public int compare(Truck truckA, Truck truckB) {                        
				return truckA.getTransmission().compareToIgnoreCase(truckB.getTransmission());
			}
		});
                }
                
                if (sort.contains("price")) {
		Collections.sort(trucks, new Comparator<Truck>() { 
			public int compare(Truck truckA, Truck truckB) {                        
				//return Integer.toString(truckA.getPrice()).compareToIgnoreCase(Integer.toString(truckB.getPrice()));
                                return truckA.getPrice() - truckB.getPrice();
			}
		});
                }
                
                if(sort.contains("desc"))
                    Collections.reverse(trucks);
		return trucks;
	}

	@Override
	public void removeTruck(String id)  {
		synchronized(this) {
			this.idToTruckMap.remove(id);
		}
	}

}

