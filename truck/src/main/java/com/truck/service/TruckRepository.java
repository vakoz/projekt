package com.truck.service;

import com.truck.domain.Truck;
import java.util.List;

public interface TruckRepository {
	Truck lookupTruckById(String id);

	void addTruck(String name, String make, String dot, String transmission,
                String extras, String description, String price);

	void updateTruck(String id, String name, String make, String dot, 
                String transmission, String extras, String description, String price);
	
	void removeTruck(String id);

	List<Truck> listTrucks(String sort);

}