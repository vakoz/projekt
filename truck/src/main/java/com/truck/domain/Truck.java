/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.truck.domain;

/**
 *
 * @author vakoz
 */
public class Truck implements Cloneable {
    private String id;
    private String name;
    private String make;
    private int dot;
    private String transmission;
    private String extras;
    private String description;
    private int price;

    public Truck(String id, String name, String make, int dot, String transmission, String extras, String description, int price) {
        this.id = id;
        this.name = name;
        this.make = make;
        this.dot = dot;
        this.transmission = transmission;
        this.extras = extras;
        this.description = description;
        this.price = price;
    }
    
    public Truck() {
        
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public int getDot() {
        return dot;
    }

    public void setDot(int dot) {
        this.dot = dot;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Truck{" + "id=" + id + ", name=" + name + ", make=" + make + ", dot=" + dot + ", transmission=" + transmission + ", extras=" + extras + ", description=" + description + ", price=" + price + '}';
    }
    
    public Truck cloneMe() {
        try {
            return (Truck) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    } 
    
}
