/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.truck.web;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author vakoz
 */
@WebServlet(name = "CountServlet", urlPatterns = {"/"})
public class CountServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                ServletContext sc = getServletContext();
                String count2 = (String)sc.getAttribute("count");
                String result;
                int count;
                if(count2 == null) {
                    count = 0;
                }
                else {
                    count = Integer.parseInt(count2);
                }
                count++;
                result = Integer.toString(count);
                sc.setAttribute("count", result);
		getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
	}

}


