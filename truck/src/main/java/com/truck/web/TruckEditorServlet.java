/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.truck.web;


/**
 *
 * @author vakoz
 */
import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.truck.domain.Truck;
import com.truck.service.TruckRepository;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class TruckEditorServlet
 */
@WebServlet("/truck/")
public class TruckEditorServlet extends HttpServlet {

	@Inject
	private TruckRepository truckRepo;
		
	
	/** Prepare the truck form before we display it. */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
                HttpSession session = request.getSession();
                

		if (id != null && !id.isEmpty()) {
			Truck truck = truckRepo.lookupTruckById(id);
			request.setAttribute("truck", truck);
		}
                else if (id == null && (session.getAttribute("name") != null) ) {
                        String name = (String)session.getAttribute("name");
                        String make = (String)session.getAttribute("make");
                        int dot = Integer.parseInt((String)session.getAttribute("dot"));
                        String transmission = (String)session.getAttribute("transmission");
                        String extras = (String)session.getAttribute("extras");
                        String description = (String)session.getAttribute("description");
                        int price = Integer.parseInt((String)session.getAttribute("price"));
                        Truck truck = new Truck(null,name,make,dot,transmission,extras,description,price);
                        request.setAttribute("truck", truck);
                }
		/* Redirect to truck-form. */
		getServletContext().getRequestDispatcher("/WEB-INF/pages/truck-form.jsp").forward(
				request, response);
                

	}
	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
                        
                HttpSession session = request.getSession();
		String name = request.getParameter("name");
		String make = request.getParameter("make");
		String dot = request.getParameter("dot");
		String transmission = request.getParameter("transmission");
                String extras = " ";
                if (request.getParameter("extras") != null)
                for (String selectedExtras : request.getParameterValues("extras")) {
                    extras += selectedExtras + ", ";
                }
                String description = request.getParameter("description");
                String price = request.getParameter("price");
		

		String id = request.getParameter("id");
                
                if (request.getParameter("submit") != null) {
		if (id == null || id.isEmpty()) {
			truckRepo.addTruck(name, make, dot, transmission, extras, 
                                description, price);
                        
                        session.setAttribute("name", name);
                        session.setAttribute("make", make);
                        session.setAttribute("dot", dot);
                        session.setAttribute("transmission", transmission);
                        session.setAttribute("extras", extras);
                        session.setAttribute("description", description);
                        session.setAttribute("price", price);
                        
                        
		} else {
			truckRepo.updateTruck(id, name, make, dot, transmission, extras, 
                                description, price);
		}
                }
                if (request.getParameter("delete") != null) {
                    truckRepo.removeTruck(id);
                }
		response.sendRedirect(request.getContextPath() + "/truck");
	}
}


