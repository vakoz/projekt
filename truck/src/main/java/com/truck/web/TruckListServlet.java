/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.truck.web;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.truck.service.TruckRepository;
import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
/**
 *
 * @author vakoz
 */

public class TruckListServlet extends HttpServlet {

	
	@Inject 
	private TruckRepository truckRepo;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                String sort = request.getParameter("sort");
                request.setAttribute("selected", sort);
                if (sort == null) {
                    sort = "id: asc";
                }
		request.setAttribute("trucks", truckRepo.listTrucks(sort));
		getServletContext().getRequestDispatcher("/WEB-INF/pages/truck-list.jsp").forward(request, response);
	}

}


