<%-- 
    Document   : truck-form
    Created on : 2014-11-18, 01:14:47
    Author     : vakoz
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE HTML>
<html>
<head>
<link rel="stylesheet" type="text/css"
      href="${pageContext.request.contextPath}/style.css" />    
<title>Truck Form</title>
</head>

<body>

	<h1>Truck Form</h1>


	<form class="semantic" method="post"
		action="${pageContext.request.contextPath}/truck/">
		<fieldset>
			<legend>
				<c:choose>
					<c:when test="${not empty truck.id }">
						Updating Truck
					</c:when>
					<c:otherwise>
						Adding Truck
					</c:otherwise>
				</c:choose>
			</legend>

			<div>
				<label for="name">Name</label> <input type="text" name="name"
					id="name" value="${truck.name}" />
			</div>
                        
                        <div>
				<label for="make">Make</label> 
                                <select id="make" name="make">
                                    <c:choose>
					<c:when test="${'Scania' ==  truck.make }">
                                            <option value="Scania" selected >Scania</option>
					</c:when>
					<c:otherwise>
                                            <option value="Scania" >Scania</option>
					</c:otherwise>
                                    </c:choose>
                                            
                                    <c:choose>
					<c:when test="${'Volvo' ==  truck.make }">
                                            <option value="Volo" selected >Volvo</option>
					</c:when>
					<c:otherwise>
                                            <option value="Volvo" >Volvo</option>
					</c:otherwise>
                                    </c:choose>
                                            
                                    <c:choose>
					<c:when test="${'Man' ==  truck.make }">
                                            <option value="Man" selected >Man</option>
					</c:when>
					<c:otherwise>
                                            <option value="Man" >Man</option>
					</c:otherwise>
                                    </c:choose>
                                    <c:choose>
					<c:when test="${'Daf' ==  truck.make }">
                                            <option value="Daf" selected >Daf</option>
					</c:when>
					<c:otherwise>
                                            <option value="Daf" >Daf</option>
					</c:otherwise>
                                    </c:choose>
                                </select>
			</div>
                        
                        <div>
				<label for="dot">Year of Production</label> <input name="dot"
					id="dot" value="${truck.dot}" />
			</div>
                        
                        <div>
				<label for="transmission">Transmission</label>                    
                                <c:choose>
					<c:when test="${'auto' ==  truck.transmission }">
                                            <input type="radio" name="transmission"
                                                   value="auto" checked />Auto <br />
                                            <input type="radio" name="transmission"
                                                   value="manual" />Manual <br />
					</c:when>
					<c:otherwise>
                                            <input type="radio" name="transmission"
                                                   value="auto" />Auto <br />
                                            <input type="radio" name="transmission"
                                                   value="manual" checked />Manual <br />
					</c:otherwise>
                                    </c:choose>
			</div>
                        
                        <div>
                            
				<label for="dot">Extras</label> 
                                
                                <c:choose>
					<c:when test="${fn:contains(truck.extras, 'ABS')}">
                                            <input type="checkbox" name="extras"
                                                   value="ABS" checked />ABS <br />
					</c:when>
					<c:otherwise>
                                            <input type="checkbox" name="extras"
                                                   value="ABS" />ABS <br />
					</c:otherwise>
                                </c:choose>
                                            
                                <c:choose>
					<c:when test="${fn:contains(truck.extras, 'power steering')}">
                                            <input type="checkbox" name="extras"
                                                   id="dot" value="power steering" checked />power steering <br />
					</c:when>
					<c:otherwise>
                                            <input type="checkbox" name="extras"
                                                   id="dot" value="power steering" />power steering <br />
					</c:otherwise>
                                </c:choose>
                                <label for="dot"></label>             
                                <c:choose>
					<c:when test="${fn:contains(truck.extras, 'GPS')}">
                                            <input type="checkbox" name="extras"
                                                   id="dot" value="GPS" checked />GPS <br />
					</c:when>
					<c:otherwise>
                                            <input type="checkbox" name="extras"
                                                   id="dot" value="GPS" />GPS <br />
					</c:otherwise>
                                </c:choose>
                                <label for="dot"></label>            
                                <c:choose>
					<c:when test="${fn:contains(truck.extras, 'heated seats')}">
                                            <input type="checkbox" name="extras"
                                                   id="dot" value="heated seats" checked />heated seats <br />
					</c:when>
					<c:otherwise>
                                            <input type="checkbox" name="extras"
                                                   id="dot" value="heated seats" />heated seats <br />
					</c:otherwise>
                                </c:choose>
                                           
                                            
			</div>
                        
			<div>
				<label for="description">Description</label>
				<textarea name="description" id="description" rows="2" cols="60">${truck.description}</textarea>
			</div>

			<div>
				<label for="price">Price $</label> <input name="price" id="price" 
					value="${truck.price}" />
			</div>


			<c:if test="${not empty truck.id}">
				<input type="hidden" name="id" value="${truck.id}" />
			</c:if>

		</fieldset>

		<div class="button-row">
			<a href="${pageContext.request.contextPath}/truck">Cancel</a> 
                        or <input type="submit" name="submit" value="submit" /> 
                        or <input type="submit" name="delete" value="delete"/> 
                        
		</div>
	</form>

</body>
</html>
