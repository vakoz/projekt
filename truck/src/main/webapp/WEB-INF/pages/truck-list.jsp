<%-- 
    Document   : truck-list
    Created on : 2014-11-18, 00:35:14
    Author     : vakoz
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE HTML>

<html>
<head>
<link rel="stylesheet" type="text/css"
      href="${pageContext.request.contextPath}/style.css" />
<title>Truck List</title>
</head>
<body>
<h1>Truck List</h1>
<a href="${pageContext.request.contextPath}/truck/">Add Truck</a>
<table class="listing">
	<tr>
		<th>id</th>
		<th>Name</th>
		<th>Make</th>
		<th>Year of Production</th>
                <th>Transmission</th>
                <th>Extras</th>
                <th>Discription</th>
                <th>Price</th>
	</tr>
	
	<c:forEach var="truck" items="${trucks}">
		<tr>
			<td>${truck.id}</td>
                        <td><a href="${pageContext.request.contextPath}/truck/?id=${truck.id}">${truck.name}</a></td>
                        <td>${truck.make}</td>
                        <td>${truck.dot}</td>
                        <td>${truck.transmission}</td>
                        <td>${truck.extras}</td>
			<td>${truck.description}</td>
			<td>${truck.price}</td>
		</tr>
	</c:forEach>
</table>


<br />
<form method="get" action="${pageContext.request.contextPath}/truck">
    sorted by: ${selected} <br />
    <select id="sort" name="sort">
        <option value="id: asc">id: asc</option>
        <option value="id: desc">id: desc</option>
        <option value="name: asc">name: asc</option>
        <option value="name: desc">name: desc</option>
        <option value="make: asc">make: asc</option>
        <option value="make: desc">make: desc</option>
        <option value="yop: asc">yop: asc</option>
        <option value="yop: desc">yop: desc</option>
        <option value="trans asc">trans: asc</option>
        <option value="trans: desc">trans: desc</option>
        <option value="price: asc">price: asc</option>
        <option value="price: desc">price: desc</option>
    </select> 
    <input type="submit" name="submit" value="submit" /> 
</form>
    
</body>
</html>
